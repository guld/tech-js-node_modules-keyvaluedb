# keyvaluedb

[![source](https://img.shields.io/badge/source-bitbucket-blue.svg)](https://bitbucket.org/guld/tech-js-node_modules-keyvaluedb) [![issues](https://img.shields.io/badge/issues-bitbucket-yellow.svg)](https://bitbucket.org/guld/tech-js-node_modules-keyvaluedb/issues) [![documentation](https://img.shields.io/badge/docs-guld.tech-green.svg)](https://guld.tech/lib/keyvaluedb.html)

[![node package manager](https://img.shields.io/npm/v/keyvaluedb.svg)](https://www.npmjs.com/package/keyvaluedb) [![travis-ci](https://travis-ci.org/guldcoin/tech-js-node_modules-keyvaluedb.svg)](https://travis-ci.org/guldcoin/tech-js-node_modules-keyvaluedb?branch=guld) [![lgtm](https://img.shields.io/lgtm/grade/javascript/b/guld/tech-js-node_modules-keyvaluedb.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/guld/tech-js-node_modules-keyvaluedb/context:javascript) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-keyvaluedb/status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-keyvaluedb) [![david-dm](https://david-dm.org/guldcoin/tech-js-node_modules-keyvaluedb/dev-status.svg)](https://david-dm.org/guldcoin/tech-js-node_modules-keyvaluedb?type=dev)

Key Value database middleware for node and the browser.

### Install

##### Node

```sh
npm i keyvaluedb
```

### Usage

Either a specific database class can be initialized, or the `defaultDB` function can be used to attempt to find a DB that works in your runtime environment.

```
const { defaultDB, memoryDB, chromeStorageDB } = require('keyvaluedb')
const db = defaultDB()
await db.set('key', 'value')
await db.get('key') // value
await db.setMany({1: 1, 2: 2})
await db.getMany([1, 2]) // {1: 1, 2: 2}
```

Then you are free to make use of the promised based `get`, `set`, `setMany` and `getMany` functions. See tests for examples.

##### Mixin

To use as an es6 class mixin, first use [object-to-class](https://bitbucket.org/isysd/object-to-class).

```
const o2c = require('object-to-class')
const MemoryDB = o2c(memoryDB, 'MemoryDB')
class MyDB extends MemoryDB {}
let mydb = new MyDB()
mydb instanceof MemoryDB // true
```

### License

MIT Copyright isysd <public@iramiller.com>
