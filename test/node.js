/* global describe:false it:false Ledger:false chai:false before:false */
const assert = require('chai').assert
const { defaultDB } = require('../src/db.js')
const db = defaultDB()

describe('node default', function () {
  it('defaultDB returns memoryDB by default', async function () {
    await db.set('hello', 'world')
    assert.exists(db._db.hello)
    assert.equal(db._db.hello, 'world')
  })
})
