/* global describe:false it:false Ledger:false chai:false before:false */
const assert = require('chai').assert
const { DBError, defaultDB } = require('../src/db.js')
const db = defaultDB()

describe ('meets standard interface', function () {
  it('set one', async function () {
    await db.set('testing', [1, 2, 3])
    assert(db._db.hasOwnProperty('testing'), 'failed to set value')
    assert(db._db.testing.length === 3, 'set value did not match expected')
    assert.equal(db._db.testing[0], 1)
    assert.equal(db._db.testing[1], 2)
    assert.equal(db._db.testing[2], 3)
  })
  it('get one', async function () {
    var vals = await db.get('testing', [1, 2, 3])
    assert(vals.length === 3, 'set value did not match expected')
    assert.equal(vals[0], 1)
    assert.equal(vals[1], 2)
    assert.equal(vals[2], 3)
  })
  it('setOverwrite one', async function () {
    await db.setOverwrite('testing', 'overwritten')
    assert.equal(db._db.testing, 'overwritten')
  })
  it('set one overwrite', async function () {
    await db.set('testing', 'overwritten again', true)
    assert.equal(db._db.testing, 'overwritten again')
  })
  it('set one do not overwrite', async function () {
    try {
      await db.set('testing', 'not overwritten', false)
    } catch (e) {
      assert.isTrue(e instanceof DBError)
      assert.equal(e.message, 'testing already exists')
    }
    assert.equal(db._db.testing, 'overwritten again')
  })
  it('setMany overwrite', async function () {
    await db.setMany({'testing': 'overwritten again', '1': 1, '2': 2, '3': 3}, true)
    assert(db._db.hasOwnProperty('testing'), 'failed to set value')
    assert(db._db.hasOwnProperty('1'), 'failed to set value')
    assert(db._db.hasOwnProperty('2'), 'failed to set value')
    assert(db._db.hasOwnProperty('3'), 'failed to set value')
    assert(Object.keys(db._db).length === 4, 'setMany value did not match expected')
    assert.equal(db._db.testing, 'overwritten again')
    assert.equal(db._db[1], 1)
    assert.equal(db._db[2], 2)
    assert.equal(db._db[3], 3)
  })
    it('setMany do not overwrite', async function () {
    try {
      await db.setMany({'testing': 'not overwritten again', '1': 2, '2': 3, '3': 4, '4': 5}, false)
    } catch (e) {
      assert.isTrue(e instanceof DBError)
      assert.isTrue(e.message.endsWith('already exists'))
    }
    assert(db._db.hasOwnProperty('testing'), 'failed to set value')
    assert(db._db.hasOwnProperty('1'), 'failed to set value')
    assert(db._db.hasOwnProperty('2'), 'failed to set value')
    assert(db._db.hasOwnProperty('3'), 'failed to set value')
    assert(Object.keys(db._db).length === 5, 'setMany value did not match expected')
    assert.equal(db._db.testing, 'overwritten again')
    assert.equal(db._db[1], 1)
    assert.equal(db._db[2], 2)
    assert.equal(db._db[3], 3)
    assert.equal(db._db[4], 5)
  })
  it('getMany', async function () {
    var vals = await db.getMany(['testing', '4'])
    assert(Object.keys(vals).length === 2, 'get  Many value did not match expected')
    assert.exists(vals.testing)
    assert.exists(vals['4'])
    assert.equal(vals.testing, 'overwritten again')
    assert.equal(vals['4'], 5)
  })
})
