/* global describe:false it:false Ledger:false chai:false before:false */
const puppeteer = require('puppeteer')
const chai = require('chai')
const path = require('path')
const { ChromeStorageDB, DBError, getBest } = require('../src/db.js')
// reused key/id from other extension since puppeteer will start fresh
const URL = 'chrome-extension://fggohbjlmjldccoelobkepngbkkebefi/test.html'

describe('ChromeStorageDB', function () {
  before(async function () {
    this.timeout(10000)
    // launch the browser, loading our extension
    // load other extensions to communicate with guld-chrome here
    browser = await puppeteer.launch({
      headless: false,
      devtools: true,
      ignoreDefaultArgs: true,
      args: args.concat([
        `--load-extension=${path.dirname(__dirname)}`
      ])
    })
    var pages = await browser.pages()
    // get the current (only) page to start manipulating
    page = pages[0]
    await page.goto(URL, { waitUntil: 'domcontentloaded' })
  })
  describe('contructor', function () {
    it('getBest returns ChromeStorageDB by default', function () {
      global.db = getBest()
      chai.assert.exists(global.db)
      chai.assert(global.db instanceof ChromeStorageDB, 'response was not a ChromeStorageDB')
    })
  })
  require('./interface.js')
})
