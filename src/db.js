/* global chrome:false */
const { memoryDB, DBError } = require('./generic.js')
const chromeStorageDB = require('./chromestorage.js')
module.exports = {
  defaultDB: () => {
    if (typeof chrome !== 'undefined' && chrome.storage) return chromeStorageDB
    else return memoryDB
  },
  memoryDB: memoryDB,
  DBError: DBError,
  chromeStorageDB: chromeStorageDB
}
