class DBError extends Error {
  constructor (message, code) {
    super(message)
    this.code = code
  }
}

async function get (key) {
  if (this._db.hasOwnProperty(key)) return this._db[key]
  else throw new DBError(`${key} not found`, 'ENOENT')
}

async function setOverwrite (key, value) {
  this._db[key] = value
}

async function set (key, value, overwrite=false) {
  if (overwrite) return this.setOverwrite(key, value)
  try {
    await this.get(key)
  } catch (e) {
    return this.setOverwrite(key, value)
  }
  throw new DBError(`${key} already exists`, 'EEXIST')
}

async function getMany (keys) {
  var results = {}
  await Promise.all(keys.map(async (k) => {
    results[k] = await this.get(k)
  }))
  var obj = {}
  for (var i = 0; i < keys.length; i++) {
    obj[i] = results[i]
  }
  return results
}

async function setMany (values, overwrite) {
  return Promise.all(Object.keys(values).map(k => {
    return this.set(k, values[k], overwrite)
  }))
}

module.exports = {
  memoryDB: {
    _db: {},
    get: get,
    setOverwrite: setOverwrite,
    set: set,
    getMany: getMany,
    setMany: setMany
  },
  DBError: DBError
}
