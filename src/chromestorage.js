/* global chrome */
const { memoryDB, DBError } = require('./generic.js')
const pify = require('pify')

module.exports = {
  get: async function (key) {
    return this.getMany([key])
  },
  setOverwrite: async function (key, value) {
    var o = {}
    o[key] = value
    return this.setMany([o])
  },
  getMany: async function (keys) {
    return new Promise((resolve, reject) => {
      chrome.storage.local.get(keys, vals => {
        if (typeof chrome.runtime.lastError !== 'undefined') { reject(chrome.runtime.lastError) } else resolve(vals)
      })
    })
  },
  setMany: async function (values, overwrite) {
    var keys = Object.keys(values)
    var got = await this.getMany(keys)
    Object.keys(got).forEach(k => {
      delete values[k]
    })
    return new Promise((resolve, reject) => {
      chrome.storage.local.set(values, err => {
        if (typeof chrome.runtime.lastError !== 'undefined') reject(new DBError(chrome.runtime.lastError, 'EIO'))
        else resolve()
      })
    })
  },
  set: memoryDB.set
}


